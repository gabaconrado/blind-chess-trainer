# Plan

Below is a development plan up to the first usable version of the system.

## V 0.1.0

- [x] Internal code structure;
- [x] Basic CLI with no arguments:
  - [x] Will automatically run the guess the color practice in the "easy" mode;
  - [x] Loop until the users enters "q";
  - [x] Will accept input "l" for light squares and "d" for dark squares;
- [x] Add unit tests;
- [x] Add traits;
- [ ] Add CLI proper argument parsing;
- [ ] Handle errors;
