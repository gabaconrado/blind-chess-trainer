/// Random number generator related traits
mod rng;

pub use self::rng::ChessRng;

#[cfg(test)]
pub use self::rng::MockChessRng;
