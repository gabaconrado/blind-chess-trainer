#[cfg(test)]
use mockall::{automock, predicate::*};
/// Defines the behavior of the Random Number Generator for a Chess Game
#[cfg_attr(test, automock)]
pub trait ChessRng {
    /// Returns a random u8 between 1 and 8 to be used as a board coordinate.
    ///
    /// Accepts optional arguments to serve as minimum and maximum.
    fn u8(&mut self, min: Option<u8>, max: Option<u8>) -> u8;
}
