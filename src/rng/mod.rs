/// Implementation of the RNG using the rand crate
pub mod rand_rng;

pub use self::rand_rng::RandRng;
