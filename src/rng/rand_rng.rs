use rand::prelude::*;

use crate::traits::ChessRng;

const DEFAULT_MIN_BOUND: u8 = 1;
const DEFAULT_MAX_BOUND: u8 = 8;

/// Struct to facilitate generation of random data. Uses the [`rand`] crate
pub struct RandRng {
    rng: ThreadRng,
}

impl RandRng {
    /// Creates a new [`RandomGen`] object
    pub fn new() -> Self {
        Self { rng: thread_rng() }
    }

    /// Checks if a value is inside the valid bound [1..8]. If not returns the provided default
    fn check_bounds(value: Option<u8>, default: u8) -> u8 {
        value.map_or(default, |v| if (1..=8).contains(&v) { v } else { default })
    }
}

impl ChessRng for RandRng {
    /// Uses the `gen_range` function from the [`Rng`] trait to generate a number between
    /// [`DEFAULT_MIN_BOUND`]..[`DEFAULT_MAX_BOUND`].
    ///
    /// If `min` and `max` are sent, uses it as range. If any is defined but it is outside the
    /// valid interval value, it will be ignored
    fn u8(&mut self, min: Option<u8>, max: Option<u8>) -> u8 {
        let lower_bound = Self::check_bounds(min, DEFAULT_MIN_BOUND);
        let upper_bound = Self::check_bounds(max, DEFAULT_MAX_BOUND);
        self.rng.gen_range(lower_bound..=upper_bound)
    }
}

impl Default for RandRng {
    fn default() -> Self {
        Self::new()
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use fixtures::*;

    mod new {
        use super::*;

        #[test]
        fn create_new_rng() {
            RandRng::new();
        }
    }

    // NOTE(gaba): The RNG trait has way too much constraints for me to be mocking it and
    // injecting the dependency in the RandRng struct to test correctly this function.
    // Because of that, these tests only calls the function a bunch of times and make sure that
    // we never get a number out of bounds. Which means it may generate false positives over
    // test runs.
    mod chess_rng {
        use super::*;

        #[test]
        fn generate_u8_invalid_bounds() {
            let run_count = 100;
            let mut rng = build_rng();
            let (min_bound, max_bound) = (10, 20);

            for _ in 0..run_count {
                let n = rng.u8(Some(min_bound), Some(max_bound));
                assert!(n <= DEFAULT_MAX_BOUND);
                assert!(n >= DEFAULT_MIN_BOUND);
            }
        }

        #[test]
        fn generate_u8_unbounded() {
            let run_count = 100;
            let mut rng = build_rng();

            for _ in 0..run_count {
                let n = rng.u8(None, None);
                assert!(n <= DEFAULT_MAX_BOUND);
                assert!(n >= DEFAULT_MIN_BOUND);
            }
        }
    }

    #[test]
    fn generate_u8_bounded() {
        let run_count = 100;
        let mut rng = build_rng();
        let (min_bound, max_bound) = (4, 5);

        for _ in 0..run_count {
            let n = rng.u8(Some(min_bound), Some(max_bound));
            assert!(n <= max_bound);
            assert!(n >= min_bound);
        }
    }

    mod fixtures {
        use super::*;

        pub fn build_rng() -> RandRng {
            RandRng::new()
        }
    }
}
