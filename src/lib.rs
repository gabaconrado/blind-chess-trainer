//! Library to provide all necessary functionality for the Blind Chess Trainer interfaces

/// Holds all practices implementation
pub mod practices;
/// Random number generator implementation
pub mod rng;
/// Internal public structs and types
pub mod structs;
/// System traits definitions
pub mod traits;
