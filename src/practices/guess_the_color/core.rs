use crate::{
    structs::{GameLevel, Square, SquareColor},
    traits::ChessRng,
};

/// Represents the "Guess The Color" chess practice
///
/// It generates a random chess square inside the given interval, and takes a guess
/// for the color of the square.
pub struct GuessTheColor {
    square: Square,
}

impl GuessTheColor {
    /// Creates a new struct with a random square
    pub fn new<T: ChessRng>(rng: &mut T, level: GameLevel) -> Self {
        let bounds = Self::get_bounds(level);
        let row = rng.u8(bounds.0, bounds.1);
        let column = rng.u8(bounds.0, bounds.1);
        let square = Square::new(row, column);
        Self { square }
    }

    /// Get a reference to the guess the color's square.
    pub fn square(&self) -> &Square {
        &self.square
    }

    /// Takes a guess for the color of the square, return true if the guess is correct,
    /// false otherwise
    pub fn guess(&self, color: SquareColor) -> bool {
        self.square.color() == color
    }

    /// Returns the chess coordinates bounds depending on the game level
    fn get_bounds(level: GameLevel) -> (Option<u8>, Option<u8>) {
        match level {
            GameLevel::Easy => (Some(1), Some(2)),
            GameLevel::Medium => (Some(1), Some(4)),
            GameLevel::Hard => (Some(1), Some(6)),
            GameLevel::GrandMaster => (None, None),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use fixtures::*;

    mod new {
        use super::*;

        #[test]
        fn new() {
            let mut rng = mock_rng();
            GuessTheColor::new(&mut rng, GameLevel::Easy);
        }

        #[test]
        fn with_level() {
            // Assertions are done inside the mocks (correct bounds)
            let _easy = build_guess_the_color_level(Some(1), Some(2), GameLevel::Easy);
            let _gtc_medium = build_guess_the_color_level(Some(1), Some(4), GameLevel::Medium);
            let _gtc_hard = build_guess_the_color_level(Some(1), Some(6), GameLevel::Hard);
            let _gtc_gm = build_guess_the_color_level(None, None, GameLevel::GrandMaster);
        }
    }

    mod getters {
        use super::*;

        #[test]
        fn square() {
            let gtc = build_guess_the_color();
            assert_eq!(gtc.square(), &gtc.square);
        }
    }

    mod guess {
        use super::*;

        #[test]
        fn guess() {
            let gtc = build_guess_the_color();
            assert!(gtc.guess(SquareColor::Dark));
            assert!(!gtc.guess(SquareColor::Light));
        }
    }

    mod fixtures {
        use super::*;
        use crate::traits::MockChessRng;
        use mockall::predicate::*;

        pub fn mock_rng() -> MockChessRng {
            let mut mock = MockChessRng::new();
            mock.expect_u8().times(2).returning(|_, _| 4);
            mock
        }

        pub fn mock_rng_args(arg_min: Option<u8>, arg_max: Option<u8>) -> MockChessRng {
            let mut mock = MockChessRng::new();
            mock.expect_u8()
                .with(eq(arg_min), eq(arg_max))
                .times(2)
                .returning(|x, _| x.unwrap_or(1));
            mock
        }

        pub fn build_guess_the_color() -> GuessTheColor {
            let mut rng = mock_rng();
            GuessTheColor::new(&mut rng, GameLevel::Easy)
        }

        pub fn build_guess_the_color_level(
            arg_min: Option<u8>,
            arg_max: Option<u8>,
            level: GameLevel,
        ) -> GuessTheColor {
            let mut rng = mock_rng_args(arg_min, arg_max);
            GuessTheColor::new(&mut rng, level)
        }
    }
}
