/// Enumeration to represent the level of the practices
#[derive(Debug, Clone, Copy)]
pub enum GameLevel {
    Easy,
    Medium,
    Hard,
    GrandMaster,
}
