/// List of values that a chess board radius can have
pub enum BoardRadius {
    /// 2x2 board [a1-2, b1-2]
    Two,
    /// 4x4 board [a1-4, b1-4, c1-4, d1-4]
    Four,
    /// 6x6 board [a1-6, b1-6, c1-6, d1-6, e1-6, f1-6]
    Six,
    /// Full board [a1-8, b1-8, c1-8, d1-8, e1-8, f1-8, g1-8, h1-8]
    Eight,
}
