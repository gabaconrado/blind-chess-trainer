use super::SquareColor;

/// Represents a square in a chess board
#[derive(Debug, PartialEq, Eq)]
pub struct Square {
    row: u8,
    column: u8,
}

impl Square {
    /// Create a new square from its coordinates
    pub fn new(row: u8, column: u8) -> Self {
        Self { row, column }
    }

    /// Return the color of a square
    pub fn color(&self) -> SquareColor {
        let coordinates_sum = self.row + self.column;
        let is_even = (coordinates_sum % 2) == 0;
        match is_even {
            true => SquareColor::Dark,
            false => SquareColor::Light,
        }
    }
}

impl std::fmt::Display for Square {
    /// Prints the square in chess notation instead of the numeric coordinates
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let col = ('a'..'h')
            .nth((self.column - 1) as usize)
            .expect("Invalid coordinate");
        write!(f, "{}{}", col, self.row)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use fixtures::*;

    mod new {
        use super::*;

        #[test]
        fn create_new_square() {
            let (row, column) = (1, 2);
            let square = Square::new(row, column);

            assert_eq!(square.row, row);
            assert_eq!(square.column, column);
        }
    }

    mod color {
        use super::*;

        #[test]
        fn return_square_color() {
            let squares_coords = [(1, 1), (4, 2), (3, 4), (8, 5)];
            let expected_colors = [
                SquareColor::Dark,
                SquareColor::Dark,
                SquareColor::Light,
                SquareColor::Light,
            ];

            for (coords, expected_color) in squares_coords.iter().zip(expected_colors) {
                let (row, column) = (coords.0, coords.1);
                let square = build_square(row, column);
                assert_eq!(square.color(), expected_color);
            }
        }
    }

    mod fixtures {
        use super::*;

        pub fn build_square(row: u8, column: u8) -> Square {
            Square::new(row, column)
        }
    }
}
