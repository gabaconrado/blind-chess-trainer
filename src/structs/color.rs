/// All colors a chess square can have
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum SquareColor {
    Light,
    Dark,
}
