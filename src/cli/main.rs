use blind_chess_trainer::{
    practices::guess_the_color::GuessTheColor,
    rng::RandRng,
    structs::{GameLevel, SquareColor},
};

fn main() {
    println!("Welcome to the blind chess trainer!");
    loop {
        let practice = GuessTheColor::new(&mut RandRng::new(), GameLevel::Easy);
        let mut guess_input = String::new();

        println!("Guess the color of the square: {}", practice.square());
        std::io::stdin()
            .read_line(&mut guess_input)
            .expect("Error reading user input");
        let guess = match guess_input.as_str().trim() {
            "l" => SquareColor::Light,
            "d" => SquareColor::Dark,
            "q" => break,
            _ => {
                println!("Invalid option");
                continue;
            }
        };

        let is_correct = practice.guess(guess);
        match is_correct {
            true => println!("Correct!"),
            false => println!("Wrong!"),
        };
    }
    println!("Bye!");
}
