# Blind Chess Trainer

> An application to help practice chess visualization skills.

## Practices

- **Guess the color**: Randoms a square from the board. Asks if it is a dark or light square;
- **How to move**: Gives you a piece, a starting square and a target square. Asks how to get the piece to the target square;

## Modes

The app has 4 modes:

- **Easy:** 2x2 board with the [a1-2, b1-2] squares;
- **Medium:** 4x4 board with the [a1-4, b1-4, c1-4, d1-4] squares;
- **Hard:** 6x6 board with the [a1-6, b1-6, c1-6, d1-6, e1-6, f1-6] squares;
- **GrandMaster:** Full 8x8 board;

## Install

TODO

## Run

TODO

## License

TODO
